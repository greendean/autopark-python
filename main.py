#!/usr/bin/env python3
import asyncio
from autopark import ParkService

park = ParkService.ParkService("fifo", "LeosArch.local:4533", 10)
while True:
    asyncio.run(park.run())

print("failed")
