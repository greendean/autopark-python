import asyncio
import os
import errno
import stat
import Hamlib


class RotatorError(Exception):
    def __init__(self, status):
        self.args = status


class ParkService:
    _socket = None
    _rotctld_addr = None
    _rot = None
    _timeout = 0

    _park_task = None

    def __init__(self, socket_name, rotctld_addr, timeout=300, port = 8988):
        self._rot = Hamlib.Rot(Hamlib.ROT_MODEL_NETROTCTL)
        self._rot.set_conf("rot_pathname", rotctld_addr)
        self._timeout = timeout

        self._socket = socket_name

    async def handle_cb(self, reader, writer):
        data = await reader.readline()

        msg = data.decode().strip()

        if msg == "start":
            if self._park_task is None or self._park_task.done():
                print("Starting park timer")
                self._park_task = asyncio.create_task(self.park_timer())

        if msg == "stop":
            if not (self._park_task is None or self._park_task.done()):
                print("Stopping park timer")
                self._park_task.cancel()

        writer.close()

    async def park(self):
        print("parking")

        self._rot.open()
        if self._rot.error_status < 0:
            print(Hamlib.rigerror(self._rot.error_status))
            raise RotatorError("")

        self._rot.park()

        self._rot.close()

    async def park_timer(self):
        await asyncio.sleep(self._timeout)
        self.park()

    async def run(self):
        server = await asyncio.start_unix_server(self.handle_cb, self._socket)

        async with server:
            await server.serve_forever()
